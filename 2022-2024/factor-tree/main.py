"""Create a factor tree given a product of prime factors."""

VALID_PRIME_NUMBER = 2
MAX_NUMBER_LENGTH = 16
INDENT_PADDING = 4


def get_user_input() -> int:
    """Get the user's input.

    The MAX_NUMBER_LENGTH limit is there to prevent large inputs from taking an
    enormous amount of time for the program to finish.

    :returns: The user's input that is a product of prime factors
    :rtype: int
    """
    while True:
        user_input = input(
            "Enter your product of prime factors to create a factor tree: "
        )
        if (
            user_input.isdigit() is True
            and int(user_input) >= VALID_PRIME_NUMBER
            and len(user_input) <= MAX_NUMBER_LENGTH
        ):
            return int(user_input)
        print(
            "The input must be an integer that is greater than one and has",
            f"less than {MAX_NUMBER_LENGTH + 1} digits",
        )


def calculate_factor_pair(product: int) -> tuple[int, int]:
    """Calculate a factor pair.

    :param product: The product of the factor pair
    :type product: int
    :returns: A pair of two factors
    :rtype: tuple[int, int]
    """
    # the start of the range is 2 because the first pair is always 1 * x
    for i in range(2, int(product**0.5) + 1):
        if product % i == 0:
            return (i, product // i)
    # if the number is prime
    return (1, product)


def is_prime(number: int) -> bool:
    """Check if a number is prime.

    :param number: The number to be checked
    :type number: int
    :returns: Whether or not the number is prime
    :rtype: bool
    """
    return not any(number % i == 0 for i in range(2, int(number**0.5) + 1))


def generate_exponential_form_equation(factors: list[int]) -> str:
    """Generate an equation in exponential form.

    The equation is a multiplication equation that results in the factors
    multiplied together.

    :param factors: The factors of the equation
    :type factors: list[int]
    :returns: An equation in exponential form
    :rtype: str
    """
    exponential_form: list[str] = []
    for index, number in enumerate(factors):
        # `index != 0` prevents a list made of entirely the same
        # numbers from being skipped
        # e.g., [2, 2], [3, 3], [17, 17]
        if index != 0 and factors[index - 1] == number:
            # skip over numbers that increased the instances variable
            # they were already accounted for
            continue

        # to prevent x ** 1
        if (instances := factors.count(number)) < 2:
            exponential_form.append(str(number))
            continue

        exponential_form.append(f"{number} ** {instances}")

    exponential_form_equation = " * ".join(exponential_form)
    return exponential_form_equation


def generate_prime_factors_product_equation(
    factors: list[int], prime_factors_product: int
) -> str:
    """Generate the equation of the product of prime factors.

    Generate what the product of prime factors is equal to in expanded and
    exponential form.

    :param factors: The factors of the equation
    :type factors: list[int]
    :param prime_factors_product: The product of the prime factors
    :type prime_factors_product: int
    :returns: The equation in expanded and exponential form
    :rtype: str
    """
    exponential_form = generate_exponential_form_equation(factors)
    expanded_form = " * ".join([str(number) for number in factors])

    # if there is no exponents, exponential form is not needed
    if "**" not in exponential_form:
        return f"{prime_factors_product} = {expanded_form}"
    return f"{prime_factors_product} = {exponential_form} = {expanded_form}"


def create_factor_tree(prime_factors_product: int):
    """Create a factor tree.

    :param prime_factors_product: The product of two prime factors
    :type prime_factors_product: int
    """
    factor_pair_product = prime_factors_product
    loop_iterations = 0
    prime_factors: list[int] = []
    print(factor_pair_product)

    while True:
        factor_pair = calculate_factor_pair(product=factor_pair_product)
        first_factor = factor_pair[0]
        prime_factors.append(first_factor)

        white_space_padding = " " * loop_iterations * INDENT_PADDING
        first_factor_space = " " * len(str(first_factor))

        print(f"{white_space_padding}|{first_factor_space}\\")
        print(white_space_padding + str(factor_pair))

        # the tree ends once the second factor is not able to be divided
        # further
        # only the second factor needs to be checked because the first factor
        # will always be the smaller one, which will always be prime or 1
        second_factor = factor_pair[1]
        if is_prime(second_factor):
            prime_factors.append(second_factor)
            print(
                generate_prime_factors_product_equation(
                    prime_factors, prime_factors_product
                )
            )
            break

        factor_pair_product = second_factor
        loop_iterations += 1


def main():
    """Run the program."""
    user_input = get_user_input()
    create_factor_tree(prime_factors_product=user_input)


if __name__ == "__main__":
    main()
