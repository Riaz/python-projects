"""
Adds a "main.py" file to all folders that do not contain one within the "future" folder. This was created so I can easily add my projects to Codeberg. Codeberg requires you to have a file within a folder to add that folder to a repository.
"""
import os
from re import S

FOLDER_PATH = os.path.join(__file__).split("/finished")[0]

SEARCH_FOLDER = f"{FOLDER_PATH}/future"
SUB_FOLDERS = [
    file.path for file in os.scandir(SEARCH_FOLDER) if file.is_dir()
]

SEARCH_FILE = "main.py"


folders_changed = []
for sub_folder in SUB_FOLDERS:
    if not os.path.exists(os.path.join(sub_folder, SEARCH_FILE)):
        with open(os.path.join(sub_folder, SEARCH_FILE), "w") as file:
            folders_changed.append(sub_folder)


get_last_folder_path = lambda folder_path: folder_path.split("/")[-1]
folders_changed = ", ".join(
    [get_last_folder_path(folder_name) for folder_name in folders_changed]
)

if folders_changed:
    (f"Folders Changed: {folders_changed}")
else:
    print("No Folders Changed")
