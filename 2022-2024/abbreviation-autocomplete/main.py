"""
Automatically types text when an abbreviation is typed.
"""

# this program does not work properly when typing in the terminal

print("Starting program . . .")

import os
import sys

import keyboard

FOLDER_PATH = os.path.dirname(os.path.realpath(__file__))

try:
    ABBREVIATIONS_PATH = f"{FOLDER_PATH}/abbreviations.txt"
    with open(ABBREVIATIONS_PATH, encoding="utf-8"):
        pass
except FileNotFoundError:
    print(
        "There is no file called abbreviations.txt in the folder. Add it so the program can run"
    )
    sys.exit()

TEXT_SEPARATION = " |!| "


def get_abbreviations() -> dict[str, str]:
    """
    Summary:
        Get the text abbreviations and their replacements from the abbreviations.txt file.

    Returns:
        dict[str, str]: The abbreviations and their replacements.
    """

    abbreviations: dict[str, str] = {}
    with open(ABBREVIATIONS_PATH, "r", encoding="utf-8") as file:
        for line_number, line in enumerate(file, 1):
            try:
                abbreviation, abbreviation_replacement = line.split(
                    TEXT_SEPARATION
                )
            except ValueError:
                print(
                    "The abbreviations and replacements in the abbreviations.txt file "
                    "do not follow the format as shown in the 'README' file. Fix the error so "
                    f"the code can run. The error is on line {line_number}"
                )
                sys.exit()
            abbreviations[abbreviation] = abbreviation_replacement

    return abbreviations


def add_abbreviation_listener(abbreviations: dict[str, str]) -> None:
    """
    Summary:
        Have the keyboard module listen for abbreviations and replace them.

    Args:
        abbreviations (dict[str, str]): The abbreviations and their replacements.
    """

    for abbreviation, abbreviation_replacement in abbreviations.items():
        keyboard.add_abbreviation(abbreviation, abbreviation_replacement)


def main() -> None:
    """
    Run the program by getting the abbreviations from the abbreviations.txt file and
    having the keyboard module listen to the abbreviations for them to be replaced.
    """

    abbreviations = get_abbreviations()
    add_abbreviation_listener(abbreviations=abbreviations)

    print("|-=-=--=-=--=-=--=-=-|")
    print("Program running!")
    try:
        keyboard.wait()
    except KeyboardInterrupt:
        print("\n|-=-=--=-=--=-=--=-=-|")
        print("Program exiting!")


if __name__ == "__main__":
    main()
