import re

import longre as long


def messageProb(
    userMessage, recognizedWord, singleResponse=False, requiredWords=[]
):
    messageCert = 0
    hasRequiredWord = True
    for word in userMessage:
        if word in recognizedWord:
            messageCert += 1
    percentage = float(messageCert) / float(len(recognizedWord))
    for word in requiredWords:
        if word not in userMessage:
            hasRequiredWord = False
            break
    if hasRequiredWord or singleResponse:
        return int(percentage * 100)
    else:
        return 0


def checkMessage(message):
    highProbList = {}

    def response(
        botResponse, listOfWords, singleResponse=False, requiredWords=[]
    ):
        nonlocal highProbList
        highProbList[botResponse] = messageProb(
            message, listOfWords, singleResponse, requiredWords
        )

    response(
        "Hello",
        ["hello", "hi", "sup", "hey", "heyo", "yo"],
        singleResponse=True,
    )
    response(
        "I'm doing fine, and you?",
        ["how", "are", "you", "doing"],
        requiredWords=["how"],
    )
    response(
        "That's great to hear!",
        ["good", "great", "amazing", "fine", "well"],
        singleResponse=True,
    )
    response(long.binaryResponse, long.binaryLetters)
    bestMatch = max(highProbList, key=highProbList.get)
    return long.unknown() if highProbList[bestMatch] < 1 else bestMatch


def response(input):
    splitMessage = re.split(r"\s+|[,;?!.-]\s*", input.lower())
    response = checkMessage(splitMessage)
    return response


while True:
    user_response = input("You: ")
    bot_response = f"Bot: {response(user_response)}"
    print(bot_response)
