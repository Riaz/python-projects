"""
Merge PDFs together
"""

import sys

import PyPDF2


def print_instructions() -> None:
    """
    Print instructions for entering PDF file paths and where the new file will
    be saved.
    """

    print("Enter the file paths of the PDFs you want to merge")
    print("Replace each space in the file path with a backslash and a space")
    print("Separate each file with a comma and a space")
    print("You can drag and drop the files to automatically enter them")
    print(
        "Be sure to enter a comma and a space before dropping in another one"
    )
    print(
        "The new PDF will be saved as merged.pdf in the directory that this "
        "script is ran in, the current folder"
    )


def merge_pdfs(pdfs: list[str]) -> None:
    """
    Merge the given PDFs.

    :param list[str] pdfs: The PDFs to merge
    :return: None
    """

    pdf_merger = PyPDF2.PdfMerger()

    for pdf in pdfs:
        try:
            pdf_merger.append(pdf)
        except FileNotFoundError:
            print(f"File not found: {pdf}")
            print("To prevent undesired results, please correct the file path")
            sys.exit(1)

    pdf_merger.write("merged.pdf")
    pdf_merger.close()

    print("The PDF has been successfully saved")


def main() -> None:
    """
    Run the program by showing the user instructions, prompting the user to
    enter their PDFs, and then merging them.
    """

    print_instructions()
    pdfs = input("Enter your PDFs: ").split(", ")
    # in case the final character is a space
    # this commonly happens if the files have been dragged in
    pdfs[-1] = pdfs[-1].removesuffix(" ")
    merge_pdfs(pdfs=pdfs)


if __name__ == "__main__":
    main()
