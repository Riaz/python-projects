"""
To run the program, run "python {file path} {the command you want to run here}." The options are save, load, and list. Saving allows you to save clipboard data with a keyword to locate the text. For example, running save would prompt you to enter a keyword. It would then save what is in your clipboard with your keyword as the key to the clipboard value. Loading allows you to grab clipboard data under a keyword. For example, running load would ask you for a keyword. It would then find that keyword in the clipboard.json file and return the clipboard value attached to it, putting it in your clipboard. List displays the clipboard.json data.
"""
import json
import os
import sys

import clipboard

FILE_PATH = os.path.join(os.path.dirname(__file__))


savedData = f"{FILE_PATH}/clipboard.json"


def saveData(filepath, data):
    with open(filepath, "w") as f:
        json.dump(data, f)


def loadData(filepath):
    try:
        with open(filepath, "r") as f:
            data = json.load(f)
            return data
    except:
        return {}


if len(sys.argv) == 2:
    command = sys.argv[1]
    data = loadData(savedData)
    if command == "save":
        key = input("Enter a key: ")
        data[key] = clipboard.paste()
        saveData(savedData, data)
        print("Data saved!")
    elif command == "load":
        key = input("Enter a key: ")
        if key in data:
            clipboard.copy(data[key])
            print("Data copied to clipboard.")
        else:
            print("Key does not exist.")
    elif command == "list":
        print(data)
    else:
        print("Unknown command")
else:
    print("Please pass exactly one command.")
